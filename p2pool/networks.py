from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(
    vootcoin=math.Object(
        PARENT=networks.nets['vootcoin'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=100, # shares
        SPREAD=30, # blocks
        IDENTIFIER='12a9e623957e4222'.decode('hex'),
        PREFIX='dfe9b8b9f1b3b5d5'.decode('hex'),
        P2P_PORT=22522,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=22521,
        BOOTSTRAP_ADDRS='voot.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-voot',
        VERSION_CHECK=lambda v: True,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
